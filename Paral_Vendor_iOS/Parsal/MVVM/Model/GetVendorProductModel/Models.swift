

import Foundation
struct Models : Codable {
	let model_files : Model_files?
	let basic_info : Basic_info?
	let category : String?
	let sub_category : [String]?
	let tested_status : Int?

	enum CodingKeys: String, CodingKey {

		case model_files = "model_files"
		case basic_info = "basic_info"
		case category = "category"
		case sub_category = "sub_category"
		case tested_status = "tested_status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		model_files = try values.decodeIfPresent(Model_files.self, forKey: .model_files)
		basic_info = try values.decodeIfPresent(Basic_info.self, forKey: .basic_info)
		category = try values.decodeIfPresent(String.self, forKey: .category)
		sub_category = try values.decodeIfPresent([String].self, forKey: .sub_category)
		tested_status = try values.decodeIfPresent(Int.self, forKey: .tested_status)
	}

}



import Foundation
struct Basic_info : Codable {
	let name : String?
	let manufacturer : String?
	let model : String?

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case manufacturer = "manufacturer"
		case model = "model"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		manufacturer = try values.decodeIfPresent(String.self, forKey: .manufacturer)
		model = try values.decodeIfPresent(String.self, forKey: .model)
	}

}

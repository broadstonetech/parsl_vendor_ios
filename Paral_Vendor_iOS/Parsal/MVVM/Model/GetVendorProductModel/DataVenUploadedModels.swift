

import Foundation
struct DataVenUploadedModels : Codable {
	let vendor_data : Vendor_data?
	let models : [Models]?

	enum CodingKeys: String, CodingKey {

		case vendor_data = "vendor_data"
		case models = "models"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		vendor_data = try values.decodeIfPresent(Vendor_data.self, forKey: .vendor_data)
		models = try values.decodeIfPresent([Models].self, forKey: .models)
	}

}



import Foundation
struct Vendor_data : Codable {
	let username : String?
	let email : String?
	let password : String?
	let vendor_name : String?
	let phone : String?
	let website : String?
	let location : Location?
	let logo_url : String?
	let category : [String]?
	let sub_category : [String]?
	let namespace : String?
	let email_verified : Int?
	let token : Token?
	let description : String?
	let slogan : String?

	enum CodingKeys: String, CodingKey {

		case username = "username"
		case email = "email"
		case password = "password"
		case vendor_name = "vendor_name"
		case phone = "phone"
		case website = "website"
		case location = "location"
		case logo_url = "logo_url"
		case category = "category"
		case sub_category = "sub_category"
		case namespace = "namespace"
		case email_verified = "email_verified"
		case token = "token"
		case description = "description"
		case slogan = "slogan"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		password = try values.decodeIfPresent(String.self, forKey: .password)
		vendor_name = try values.decodeIfPresent(String.self, forKey: .vendor_name)
		phone = try values.decodeIfPresent(String.self, forKey: .phone)
		website = try values.decodeIfPresent(String.self, forKey: .website)
		location = try values.decodeIfPresent(Location.self, forKey: .location)
		logo_url = try values.decodeIfPresent(String.self, forKey: .logo_url)
		category = try values.decodeIfPresent([String].self, forKey: .category)
		sub_category = try values.decodeIfPresent([String].self, forKey: .sub_category)
		namespace = try values.decodeIfPresent(String.self, forKey: .namespace)
		email_verified = try values.decodeIfPresent(Int.self, forKey: .email_verified)
		token = try values.decodeIfPresent(Token.self, forKey: .token)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		slogan = try values.decodeIfPresent(String.self, forKey: .slogan)
	}

}

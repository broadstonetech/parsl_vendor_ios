

import Foundation
struct GetVendorProductModel : Codable {
	let message : String?
	let data : DataVenUploadedModels?
	let status : Bool?

	enum CodingKeys: String, CodingKey {

		case message = "message"
		case data = "data"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		data = try values.decodeIfPresent(DataVenUploadedModels.self, forKey: .data)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
	}

}

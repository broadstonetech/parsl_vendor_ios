

import Foundation
struct Model_files : Codable {
	let thumbnail : String?

	enum CodingKeys: String, CodingKey {

		case thumbnail = "thumbnail"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		thumbnail = try values.decodeIfPresent(String.self, forKey: .thumbnail)
	}

}

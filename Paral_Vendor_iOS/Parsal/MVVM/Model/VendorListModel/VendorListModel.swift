//
//  VendorListModel.swift
//  Parsal
//
//  Created by Naqash Ali on 02/03/2022.
//





import Foundation
struct VendorListModel : Codable {
    let message : String?
    let data : DataVen?
    let status : Bool?

    enum CodingKeys: String, CodingKey {

        case message = "message"
        case data = "data"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(DataVen.self, forKey: .data)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
    }

}
struct DataVen : Codable {
    let vendors_list : [Vendors_list]?

    enum CodingKeys: String, CodingKey {

        case vendors_list = "vendors_list"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        vendors_list = try values.decodeIfPresent([Vendors_list].self, forKey: .vendors_list)
    }

}

// MARK: - Vendor List
struct Vendors_list : Codable {
    let username : String?
    let email : String?
    let password : String?
    let vendor_name : String?
    let phone : String?
    let website : String?
    let location : Location?
    let logo_url : String?
    let category : [String]?
    let sub_category : [String]?
    let namespace : String?
    let email_verified : Int?
    let token : Token?
    let description : String?
    let slogan : String?

    enum CodingKeys: String, CodingKey {

        case username = "username"
        case email = "email"
        case password = "password"
        case vendor_name = "vendor_name"
        case phone = "phone"
        case website = "website"
        case location = "location"
        case logo_url = "logo_url"
        case category = "category"
        case sub_category = "sub_category"
        case namespace = "namespace"
        case email_verified = "email_verified"
        case token = "token"
        case description = "description"
        case slogan = "slogan"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        vendor_name = try values.decodeIfPresent(String.self, forKey: .vendor_name)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        location = try values.decodeIfPresent(Location.self, forKey: .location)
        logo_url = try values.decodeIfPresent(String.self, forKey: .logo_url)
        category = try values.decodeIfPresent([String].self, forKey: .category)
        sub_category = try values.decodeIfPresent([String].self, forKey: .sub_category)
        namespace = try values.decodeIfPresent(String.self, forKey: .namespace)
        email_verified = try values.decodeIfPresent(Int.self, forKey: .email_verified)
        token = try values.decodeIfPresent(Token.self, forKey: .token)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        slogan = try values.decodeIfPresent(String.self, forKey: .slogan)
    }

}

struct Token : Codable {

//    enum CodingKeys: String, CodingKey {
//
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//    }

}


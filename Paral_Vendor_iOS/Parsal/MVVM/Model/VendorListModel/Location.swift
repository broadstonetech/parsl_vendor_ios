

import Foundation
struct Location : Codable {
	let country_name : String?
	let city : String?
	let state : String?
	let zip_code : String?
	let lattitude : Double?
	let longitude : Double?

	enum CodingKeys: String, CodingKey {

		case country_name = "country_name"
		case city = "city"
		case state = "state"
		case zip_code = "zip_code"
		case lattitude = "lattitude"
		case longitude = "longitude"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		state = try values.decodeIfPresent(String.self, forKey: .state)
		zip_code = try values.decodeIfPresent(String.self, forKey: .zip_code)
		lattitude = try values.decodeIfPresent(Double.self, forKey: .lattitude)
		longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
	}

}

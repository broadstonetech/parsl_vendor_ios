//
//  Manager.swift
//  Parsal
//
//  Created by Hussnain Ali on 22/02/2022.
//

import Foundation
import UIKit
struct infoDict{
    var tag : String = ""
    var name : String = ""
    var email : String = ""
    var discription : String = ""
    var phone : String = ""
}
struct sourceDict{
    var tag : String = ""
    var source : String = ""
    var price : String = ""
    var first : String = ""
    var second : String = ""
}
struct locationDict{
    var tag : String = ""
    var city : String = ""
    var state : String = ""
    var zipCode : String = ""
    var country : String = ""
}
struct keyDict{
    var tag : String = ""
    var key : String = ""
    var value : String = ""
  
}

class Helpers{
   
    static var detailsOfNED : infoDict?
    static var detailsOfSource : sourceDict?
    static var detailsOfLocation : locationDict?
    static var arrOfKeys : [keyDict] = [keyDict]()
    static var cat : String = "Choose Category"
    static var pickedCat : String = ""
    static var subCat : String = "Choose Sub Category"
    static var thumbnail_image: UIImage!
    static var destinationURL: String!
    static var size: String!
    static var comeFrom : Bool = false
    static var checkAttachedImages : Bool = false
    static var imageFolderName : String = ""
    static var nameSpace : String = ""
    static var dailCode : String!
    static var isAddedNewThing : Bool = false
    static var selectedVenCat : String!
}

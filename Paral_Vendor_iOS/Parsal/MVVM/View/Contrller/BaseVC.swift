//
//  BaseVC.swift
//  Parsal
//
//  Created by Hussnain Ali on 23/02/2022.
//

import UIKit
import Alamofire

class BaseVC: UIViewController {
    
    
    
    
    //MARK: - SHOW TOAST
    
    func presentToast(message : String, font: UIFont) {
      
        let textWidth = (message.size(withAttributes:[.font: UIFont.systemFont(ofSize: 14)]).width ) + 20
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - (textWidth / 2), y: self.view.frame.size.height - 100, width: textWidth, height: 40))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func showLongTaskToast(message : String, font: UIFont){
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 125, y: self.view.frame.size.height-100, width: 250, height: 50))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 30, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
    //MARK:  - DISSMISS KEYBOAD
    func dismissMyKeyboard(){
        view.endEditing(true)
    }
    
    //MARK:- ALERT SHOW
    func showAlert(_ message:String)
    {
        
        let alertController = UIAlertController(title: "Parsal Vendor", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        
        let messageText = NSMutableAttributedString(
            string: message,
            attributes: [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0)
            ]
        )
        
        alertController.setValue(messageText, forKey: "attributedMessage")
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK:- Webservice Calling Functions
    
    func postRequest(serviceName: String,sendData : [String : Any],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {
        print("url == \(serviceName)")
        print("params == \(sendData)")
        // ParsalUtilities.sharedInstance.showProgressHUD()
        let serviceUrl =  serviceName
        print("url == \(serviceUrl)")
        //print("url-1 == \(ConstantStrings.aapModeBaseURl)")
        let url = NSURL(string: serviceUrl)
        //  let url = NSURL(string: "https://api.parsl.io/add/vendor/product")
        //create the session object
        let session = URLSession.shared
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST" //set http method as POST
        //        request.timeoutInterval = 20
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            print(" response is \(response)")
            
            //copied
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
                UserDefaults.standard.synchronize()
                
                if(httpStatus.statusCode == 401)
                {
                    NetworkHelper.sharedInstance.error401 = true
                }
                else if(httpStatus.statusCode == 403)
                {
                    NetworkHelper.sharedInstance.error403 = true
                }
                
            }
            let httpStats = response as? HTTPURLResponse
            UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
            UserDefaults.standard.synchronize()
            //end copied
            
            guard error == nil else {
                print("this is error")
                DispatchQueue.main.async {
                    ParsalUtilities.sharedInstance.dismissProgressHUD()
                    self.showAlert(ConstantStrings.internet_off)
                    
                }
                return
            }
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    print("json is \(json)")
                    NetworkHelper.sharedInstance.isConnectedDevicesSuccess = true
                    success(json)
                    //handle json...
                    //ParsalUtilities.sharedInstance.dismissProgressHUD()
                }
                
                //CozyLoadingActivity.hide(true, animated: false)
            } catch let error {
                //  ParsalUtilities.sharedInstance.dismissProgressHUD()
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
            
        })
        
        task.resume()
        
    }
    
    //MARK: - UPLOAD FILES TO SERVER
    func uploadFileToServer(path: String,size: String,product_id : String,success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void){
        let url = URL(string: "https://api.parsl.io/add/vendor/product/files")!
        let image = Helpers.thumbnail_image
        let imgData = image!.jpegData(compressionQuality: 0.5)!
        showLongTaskToast(message: "Dont quite untill completion.", font:  .systemFont(ofSize: 14.0))
        Alamofire.upload(multipartFormData: { multipart in
            multipart.append(NSURL(fileURLWithPath: path) as URL, withName: "zipfile")
            multipart.append(size.data(using: .utf8)!, withName :"size")
            multipart.append(Helpers.nameSpace.data(using: .utf8)!, withName :"namespace")
            multipart.append(product_id.data(using: .utf8)!, withName :"product_id")
            multipart.append(imgData, withName: "thumbnail_file",fileName: "thumbnail_file.jpg", mimeType: "image/jpg")
        }, to: url, method: .post, headers: nil) { encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.response { answer in
                    print("completed",upload.response as Any)
                    
                    if answer.response?.statusCode == 200 {
                        DispatchQueue.main.async {
                            Helpers.isAddedNewThing = true
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                    }else if answer.response?.statusCode == 503 {
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            self.showAlert(ConstantStrings.ERROR_TEXT_503)
                        }
                    }else if answer.response?.statusCode == 409 {
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            self.showAlert(ConstantStrings.ERROR_TEXT_409)
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            self.showAlert(ConstantStrings.ErrorText)
                        }
                    }
                }
                upload.uploadProgress { progress in
                    //call progress callback here if you need it
                    print(progress)
                }
            case .failure(let encodingError):
                DispatchQueue.main.async {
                    ParsalUtilities.sharedInstance.dismissProgressHUD()
                    self.showAlert("multipart upload encodingError: \(encodingError)")
                }
            }
        }
    }
    
    
    
    
}




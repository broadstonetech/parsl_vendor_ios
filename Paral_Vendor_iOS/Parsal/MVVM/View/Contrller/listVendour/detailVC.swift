//
//  detailVC.swift
//  Parsal
//
//  Created by Hussnain Ali on 23/02/2022.
//

import UIKit
import SwiftUI

class detailVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var detailTableView: UITableView!
    @IBOutlet weak var lblEmptyProducts: UILabel!
    
    
    
    
    
    //MARK: - Variables
    
    var vendorName = ""
    var nameSpace = ""
    
    var vendorProducts:GetVendorProductModel?
    var venProData  =  [Models]()
    var vendModels = [Models]()
    
    
    var isDeActiveShow = true
    var statusStyleTitle = "Status (Active)"
    
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .gray
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        
        return refreshControl
    }()
    
    //MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addDoneButtonOnKeyboard()
        self.lblEmptyProducts.isHidden = true
        self.navigationItem.title = vendorName
        let rightButton = UIBarButtonItem(title: "Create Model", style: UIBarButtonItem.Style.bordered, target: self, action:  #selector(create3DModel(sender:)))
        self.navigationItem.rightBarButtonItem = rightButton
        detailTableView.delegate = self
        detailTableView.dataSource = self
        
        detailTableView.refreshControl = refresher
        detailTableView.addSubview(refresher)
        
        searchBar.delegate = self
        self.getVendorProducts()
        
    }
    override func viewWillAppear(_ animated: Bool) {
     
        if Helpers.isAddedNewThing {
            self.getVendorProducts()
            Helpers.isAddedNewThing = false
        }
    }
    
    //MARK: - Button Action
    
    @IBAction func filterBtn(_ sender: Any) {
        self.actionSheet(sender: sender)
    }
    
    
    //MARK: - Functions
    @objc func create3DModel(sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
 
    @objc func reloadData() {
        
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
        
            self.getVendorProducts()
            self.refresher.endRefreshing()
        }
    }
    
    //MARK: - Add Done on KeyBoard
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))

        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)

        doneToolbar.items = items
        doneToolbar.sizeToFit()

        self.searchBar.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction() {
        self.searchBar.resignFirstResponder()
    }
    //MARK: -  APi Function
    func getVendorProducts() {
        if ParsalUtilities.sharedInstance.connected() == false{
            return
        }
        ApiHandler.sharedInstance.getVendorAllProduct(namespace: self.nameSpace, controller: self) { result, resp in
            if result {
                
                let msg = resp?.message
                
                if msg == "Vendor products found"{
                    self.lblEmptyProducts.isHidden = true
                    self.vendorProducts = resp
                    self.venProData = (resp?.data?.models)!
                    self.vendModels = (resp?.data?.models)!
                    
                   
                    
                    self.detailTableView.reloadData()
                }else if msg == "Vendor products not found." ||  msg == "No vendor data found." {
                    self.lblEmptyProducts.isHidden = false
                }else{
                    debugPrint(resp)
                }
            }
        }
        
    }
}

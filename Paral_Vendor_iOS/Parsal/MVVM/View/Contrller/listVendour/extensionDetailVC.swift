//
//  extensionDetailVC.swift
//  Parsal
//
//  Created by Hussnain Ali on 23/02/2022.
//

import Foundation
import UIKit
import SDWebImage

extension detailVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.vendModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = detailTableView.dequeueReusableCell(withIdentifier: "modelCell") as! modelCell
        
        
        let obj = vendModels
        let productObj = obj[indexPath.row]
        
        cell.nameLbl.text = productObj.basic_info?.name
        
        
        if productObj.tested_status == 1 {
            cell.statusLbl.text = "Active"
        } else{
            cell.statusLbl.text = "Inactive"
        }
        
        
        cell.subCategoryLbl.text = productObj.category?.capitalized
        cell.modelPorfileImg.sd_setImage(with: URL(string: (productObj.model_files?.thumbnail)! ), placeholderImage: UIImage(named: "img_placeholder"))
        
        if isDeActiveShow == true {
            
            cell.bgView.isHidden =  false
        }else{
            cell.bgView.isHidden =  true
        }
        
        
        
        return cell
    }
    
    
    
    
    //MARK: - ACTICTIONSHEET
    
    func actionSheet(sender: Any){
        // Create the AlertController
        let actionSheetController = UIAlertController(title: "Please select", message: "", preferredStyle: .actionSheet)
        
        // Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            // Just dismiss the action sheet
            self.dismiss(animated: true, completion: nil)
        }
        actionSheetController.addAction(cancelAction)
        
        // Create and add first option action
        let ascending = UIAlertAction(title: "Ascending (Name)", style: .default) { action -> Void in
            
            
            self.vendModels = self.vendModels.sorted(by: {
                $0.basic_info?.name ?? "" < $1.basic_info?.name ?? ""
            })
            DispatchQueue.main.async {
                self.detailTableView.reloadData()
            }
        }
        actionSheetController.addAction(ascending)
        
        // Create and add a second option action
        let descending = UIAlertAction(title: "Descending (Name)", style: .default) { action -> Void in
            
            self.vendModels = self.vendModels.sorted(by: {
                $0.basic_info?.name ?? "" > $1.basic_info?.name ?? ""
            })
            DispatchQueue.main.async {
                self.detailTableView.reloadData()
            }
        }
        actionSheetController.addAction(descending)
        
        // Create and add a third option action
        let status = UIAlertAction(title: self.statusStyleTitle, style: .default) { action -> Void in
            
            if  self.isDeActiveShow {
                
                self.isDeActiveShow = false
                DispatchQueue.main.async {
                    self.detailTableView.reloadData()
                }
                self.statusStyleTitle = "Status (All)"
            }else{
                self.isDeActiveShow = true
                DispatchQueue.main.async {
                    self.detailTableView.reloadData()
                }
                self.statusStyleTitle = "Status (Active)"
                
            }
            
            
        }
        actionSheetController.addAction(status)
        
        
        // We need to provide a popover sourceView when using it on iPad
        actionSheetController.popoverPresentationController?.sourceView = sender as? UIView
        
        // Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
}
extension detailVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if searchBar.text != "" {
            self.vendModels.removeAll()
            self.vendModels = self.venProData.filter ({$0.basic_info!.name!.lowercased().contains(searchBar.text!.lowercased()) })
            detailTableView.reloadData()
        }else{
            self.vendModels.removeAll()
            
            
            if let model = self.vendorProducts?.data?.models {
                self.vendModels = model
            }
            
            detailTableView.reloadData()
        }
    }
}

//
//  listVendourVc.swift
//  Parsal
//
//  Created by Hussnain Ali on 22/02/2022.
//

import UIKit
import EFInternetIndicator
import SwiftMessages

class listVendourVc: UIViewController,InternetStatusIndicable {
    
    
    
    //MARK: - OutLets
    @IBOutlet weak var tableVu: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterBtn: UIButton!
    
    //MARK: - Variables
    var internetConnectionIndicator: InternetViewIndicator?
    var name : [String] = []
    var allData = ["Hussnain Ali","Zaryab","Umar Farooq","Mubashar","Mabashar","m","me","mo","mu"]
    
    var arrVendorData = [[String:Any]]()
    
    var vendorObj:VendorListModel?
    var nonFilterData = [Vendors_list]()
    var vendListObj = [Vendors_list]()
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .gray
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        
        return refreshControl
    }()
    
    
    //MARK: -  Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startMonitoringInternet()
        self.addDoneButtonOnKeyboard()
        name = allData
        let rightButton = UIBarButtonItem(title: "Add New", style: UIBarButtonItem.Style.plain, target: self, action:  #selector(creatNewVendour(sender:)))
        self.navigationItem.rightBarButtonItem = rightButton
        
        tableVu.refreshControl = refresher
        tableVu.addSubview(refresher)
        
        tableVu.delegate = self
        tableVu.dataSource  = self
        
        tableVu.rowHeight = UITableView.automaticDimension
        searchBar.delegate = self
        self.getVendorList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Helpers.isAddedNewThing {
            self.getVendorList()
            Helpers.isAddedNewThing = false
        }
        
        
    }
    
    //MARK: - Button Actions
    
    @IBAction func filterBtn(_ sender: Any) {
       
        self.actionSheet(sender: sender)
    }
    
    
    //MARK: - Functions
    @objc func reloadData() {
        
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.getVendorList()
            self.refresher.endRefreshing()
        }
    }
    
    @objc func creatNewVendour(sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "createVendourVC") as! createVendourVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Add Done on KeyBoard
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.searchBar.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.searchBar.resignFirstResponder()
    }
    
    
    //MARK: - Api Function
    func getVendorList() {
        if ParsalUtilities.sharedInstance.connected() == false{
            return
        }
        ApiHandler.sharedInstance.getAllVendors(controller: self) { result, resp  in
            
            if result {
                
                
                
                if resp?.message == "Found the vendor."{
                    self.vendorObj = resp
                    self.vendListObj = (resp?.data?.vendors_list)!
                    self.nonFilterData = (resp?.data?.vendors_list)!
                    DispatchQueue.main.async {
                        
                        self.tableVu.reloadData()
                    }
                    
                }else{
                    debugPrint(resp)
                }
            }
        }
    }
    
}




//
//  ViewController.swift
//  Parsal
//
//  Created by Hussnain Ali on 18/02/2022.
//

import UIKit


class ViewController: BaseVC{
    
    var show = true
    
    @IBOutlet weak var tableVu: UITableView!
    
    var arr = ["1"]
    //imagePickerControlleR
    let imagePicker = UIImagePickerController()
    var lastSelectedIndex: Int?
    let dictCatData = ["decor":"Decor",
                       "electronics":"Electronics",
                       "architecture":"Architecture",
                       "fashion":"Fashion",
                       "food":"Food",
                       "special_occasions":"Special Occasions"]
    //MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helpers.detailsOfSource =  nil
        Helpers.detailsOfLocation = nil
        Helpers.arrOfKeys.removeAll()
        Helpers.arrOfKeys.append(keyDict(tag: "", key: "", value: ""))
        
        Helpers.cat = self.dictCatData[Helpers.pickedCat] ?? ""
        Helpers.subCat = "Choose Sub Category"
        Helpers.thumbnail_image = nil
        Helpers.destinationURL = nil
        Helpers.size = nil
        Helpers.comeFrom = false
        Helpers.checkAttachedImages = false
        Helpers.imageFolderName = ""
        let rightButton = UIBarButtonItem(title: "Save", style: UIBarButtonItem.Style.plain, target: self, action:  #selector(save(sender:)))
        self.navigationItem.rightBarButtonItem = rightButton
        self.navigationItem.title = "Create 3D Model"
        tableVu.delegate = self
        tableVu.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        _ = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
     
        
    }
    
    //MARK: - Functions
    @objc func update() {
        tableVu.reloadData()
        
    }
    @objc func save(sender: UIBarButtonItem) {
        self.dismissMyKeyboard()
        
        if Helpers.thumbnail_image == nil {
            self.presentToast(message: ConstantStrings.Slogan, font: .systemFont(ofSize: 14.0))
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
        }else  if Helpers.detailsOfNED?.name == nil || Helpers.detailsOfNED?.discription == nil ||   Helpers.detailsOfSource?.price == nil || Helpers.detailsOfSource?.first == nil ||  Helpers.detailsOfNED?.name == "" || Helpers.detailsOfNED?.discription == "" ||   Helpers.detailsOfSource?.price == "" || Helpers.detailsOfSource?.first == ""{
            
            self.presentToast(message: ConstantStrings.formFill, font: .systemFont(ofSize: 14.0))
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
        }else if Helpers.cat == "Choose Category"{
            self.presentToast(message: ConstantStrings.catSection, font: .systemFont(ofSize: 14.0))
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
        }else if  Helpers.subCat == "Choose Sub Category" {
            self.presentToast(message: ConstantStrings.subCatSection, font: .systemFont(ofSize: 14.0))
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
        }else if  Helpers.destinationURL == nil{
            self.presentToast(message: ConstantStrings.imageSection, font: .systemFont(ofSize: 14.0))
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
        }else{
            self.createModel()
        }
        
        
    }
    //MARK: - Create Model API
    func createModel(){
        if ParsalUtilities.sharedInstance.connected() == false{
            
            return
        }
        
        ParsalUtilities.sharedInstance.showProgressHUD()
        
        
        
        let timeInterval = NSDate().timeIntervalSince1970
        var data = [AnyObject]()
        for item in Helpers.arrOfKeys{
            let dict = ["tag": item.tag,
                        "key": item.key,
                        "value": item.value ] as [String: AnyObject]
            data.append(dict as AnyObject)
        }
        let params: [String: Any] = ["namespace": Helpers.nameSpace,
                                     "name" : Helpers.detailsOfNED!.name,
                                     "manufacturer": "LocusAR",
                                     "model": "Snow Monster",
                                     "category" : Helpers.cat,
                                     "description": Helpers.detailsOfNED!.discription,
                                     "manufacturing_date":"\(Int64(timeInterval))",
                                     "license":"Open source",
                                     "price":Helpers.detailsOfSource!.price as Any,
                                     "price_currency":"USD",
                                     "price_status":"free",
                                     "scale_unit":"inches",
                                     "min_x_axis":"2",
                                     "min_y_axis":"2",
                                     "min_z_axis":"2",
                                     "max_x_axis":"50",
                                     "max_y_axis":"50",
                                     "max_z_axis":"50",
                                     "action_type":"link",
                                     "action_title":"Read More",
                                     "action_data": Helpers.detailsOfSource!.first,
                                     "license_source":Helpers.detailsOfSource!.source,
                                     "sub_category":Helpers.subCat,
                                     "additional_info": data,
                                     "model_of": "both"] as [String: AnyObject]
        
        postRequest(serviceName: ConstantStrings.add_Vendour_Model_texture, sendData: params, success: { (data) in
            let me = data["data"]!
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            if ((statusCode?.range(of: "200")) != nil){
                //go to discovery page
                if me["product_id"] as? String != nil &&  me["product_id"] as? String != ""{
                    DispatchQueue.main.async{
                                     self.uploadFileToServer(path: Helpers.destinationURL, size: Helpers.size!, product_id: me["product_id"] as! String) { data in
                                        // debugPrint(data)
                                     } failure: { data in
                                         debugPrint("fail")
                                     }
                                 }
                }
             
            }else if ((statusCode?.range(of: "202")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        ParsalUtilities.sharedInstance.dismissProgressHUD()
                        self.showAlert(msg)
                    }
                }else{
                    DispatchQueue.main.async {
                        ParsalUtilities.sharedInstance.dismissProgressHUD()
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        ParsalUtilities.sharedInstance.dismissProgressHUD()
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        ParsalUtilities.sharedInstance.dismissProgressHUD()
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                ParsalUtilities.sharedInstance.dismissProgressHUD()
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        ParsalUtilities.sharedInstance.dismissProgressHUD()
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if (data["message"] != nil) {
                
                let message = data["message"] as! String
               // debugPrint(message)
                DispatchQueue.main.async {
                    ParsalUtilities.sharedInstance.dismissProgressHUD()
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    ParsalUtilities.sharedInstance.dismissProgressHUD()
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
        }, failure: { (data) in
           // debugPrint(data,"++==")
            DispatchQueue.main.async {
                ParsalUtilities.sharedInstance.dismissProgressHUD()
                self.showAlert(data["Error"] as! String)
                
                // self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    
}





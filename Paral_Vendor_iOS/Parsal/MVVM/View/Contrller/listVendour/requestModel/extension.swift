//
//  extension.swift
//  Parsal
//
//  Created by Hussnain Ali on 23/02/2022.
//

import Foundation
import UIKit
import SwiftUI
//MARK: - TABLE VIEW EXTENSION
extension ViewController:  UITableViewDelegate ,UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate,SelectionDelegate {
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Helpers.arrOfKeys.count + 5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        arr =  arr.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
        
        if indexPath.row == 0{
            let cell = tableVu.dequeueReusableCell(withIdentifier: "uploadCell", for: indexPath) as! uploadImageCell
            tableVu.rowHeight = UITableView.automaticDimension
            tableVu.estimatedRowHeight = 140
            cell.uploadBtn.tag = indexPath.row
            cell.uploadBtn.addTarget(self, action: #selector(imageUpload(sender:)), for: .touchUpInside)
          
            return cell
        }else if indexPath.row == 1 {
            let cell = tableVu.dequeueReusableCell(withIdentifier: "fieldCell", for: indexPath) as! twoFieldsCell
            tableVu.rowHeight = UITableView.automaticDimension
            tableVu.estimatedRowHeight = 128
            cell.nameTf.tag = indexPath.row
            cell.textView.tag = indexPath.row
            return cell
        }else if indexPath.row == 2 {
            let cell = tableVu.dequeueReusableCell(withIdentifier: "sourceCell", for: indexPath) as! sourceCell
            tableVu.rowHeight = UITableView.automaticDimension
            tableVu.estimatedRowHeight = 200
            cell.secondBlankTf.tag = indexPath.row
            cell.firstBlankTf.tag = indexPath.row
            cell.sourceTf.tag = indexPath.row
            cell.priceTf.tag = indexPath.row
            return cell
        }
 //           else if indexPath.row == 3 {
//            let cell = tableVu.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath) as! locationCell
//            tableVu.rowHeight = UITableView.automaticDimension
//            tableVu.estimatedRowHeight = 207
//            cell.cityTf.tag = indexPath.row
//            cell.stateTf.tag = indexPath.row
//            cell.zipCodeTf.tag = indexPath.row
//            cell.countryTf.tag = indexPath.row
//            return cell
//        }
        else if indexPath.row == 3 {
            let cell = tableVu.dequeueReusableCell(withIdentifier: "catCell", for: indexPath) as! catCell
            tableVu.rowHeight = UITableView.automaticDimension
            tableVu.estimatedRowHeight = 144
            cell.catBtn.setTitle(Helpers.cat, for: .normal)
            cell.catBtn.addTarget(self, action: #selector(catPicker(sender:)), for: .touchUpInside)
            cell.subCatBtn.addTarget(self, action: #selector(subCatPicker(sender:)), for: .touchUpInside)
        
            return cell
        }else if indexPath.row == arr.count + 4 {
            let  cell = tableVu.dequeueReusableCell(withIdentifier: "bottomCell", for: indexPath) as! bottomCell
            tableVu.rowHeight = UITableView.automaticDimension
            tableVu.estimatedRowHeight = 16
            cell.addImages.tag = indexPath.row
            cell.addImages.addTarget(self, action: #selector(addImages(sender:)), for: .touchUpInside)
            if Helpers.checkAttachedImages == true{
                cell.LblFolder.isHidden = false
                cell.LblFolder.text = Helpers.imageFolderName
                cell.addImages.setTitle("Images are attached", for: .normal)
            debugPrint(Helpers.imageFolderName)
            }else{
                cell.addImages.setTitle("Select Images", for: .normal)
                cell.LblFolder.isHidden = true
                
            }
            return cell
        }else{
           // DispatchQueue.main.async { [self] in
            let  cell = tableVu.dequeueReusableCell(withIdentifier: "addCell", for: indexPath) as! addCell
            cell.addBtn.tag = indexPath.row
            cell.addBtn.addTarget(self, action: #selector(addOneMore(sender:)), for: .touchUpInside)
            cell.removeBtn.tag = indexPath.row
            cell.removeBtn.addTarget(self, action: #selector(removeOne(sender:)), for: .touchUpInside)
          
            cell.keyTf.tag = indexPath.row
            cell.valueTf.tag = indexPath.row
            cell.keyTf.delegate = self
            cell.valueTf.delegate = self
            cell.keyTf.placeholder = "Key"
            if  arr[indexPath.row - 4] == "1"{
                cell.LblHeading.isHidden = false
            }else{
                cell.LblHeading.isHidden = true
            }
            if Helpers.arrOfKeys[indexPath.row - 4].key != ""{
                cell.keyTf.text = Helpers.arrOfKeys[indexPath.row - 4].key
            }else{
                cell.keyTf.text = ""
            }
            if Helpers.arrOfKeys[indexPath.row - 4].value != ""{
                cell.valueTf.text = Helpers.arrOfKeys[indexPath.row - 4].value
            }else{
                cell.valueTf.text = ""
            }
                if indexPath.row  == arr.count + 3 {
                    cell.addBtn.isHidden = false
                    cell.removeBtn.isHidden = true
                }else{
                    cell.addBtn.isHidden = true
                    cell.removeBtn.isHidden = false
                }

            return cell
            }
       
            
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    //MARK: - PICKER BUTTON
    @objc func catPicker(sender: UIButton){
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "pickerVC") as! pickerVC
        vc.type = "cat"
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
        vc.selectionDelegate = self

    }
    @objc func subCatPicker(sender: UIButton){
        let vc = storyboard?.instantiateViewController(withIdentifier: "pickerVC") as! pickerVC
        vc.type = "subCat"
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
        vc.selectionDelegate = self
 }
    func tabForData(type:String) {
       
        let indexPath = IndexPath(row: 3, section: 0)
        let cell = self.tableVu.cellForRow(at: indexPath) as! catCell
        if type == "cat" {
            cell.catBtn.setTitle(Helpers.cat, for: .normal)
            cell.catBtn.setTitleColor(.black, for: .normal)
            
            cell.subCatBtn.setTitle( Helpers.subCat, for: .normal)
            cell.subCatBtn.setTitleColor(.lightGray, for: .normal)
        }else{
            cell.subCatBtn.setTitle( Helpers.subCat, for: .normal)
            cell.subCatBtn.setTitleColor(.black, for: .normal)
        }
       
        
    }
     
    //MARK: - UPLOADIMAGE
    @objc func imageUpload(sender: UIButton){
        
        
        let tab = UIAlertController(title:nil, message: nil, preferredStyle:
                                            .actionSheet)
            let camera = UIAlertAction(title: "Camera", style: .default) { (alert:UIAlertAction) in
                self.imagePicker.sourceType = .camera
                self.imagePicker.delegate = self
                self.imagePicker.isEditing = true
        //        imagePicker.modalTransitionStyle = .flipHorizontal
        //        imagePicker.modalPresentationStyle = .fullScreen
                self.present(self.imagePicker, animated: true, completion: nil)
                
            }
            let PhotoLibrary = UIAlertAction(title: "Photo Library", style: .default) { (alert:UIAlertAction) in

                let buttonTag = sender.tag
                self.imageSelection(tag: buttonTag)
               // debugPrint(sender.tag)
                
            }
            let cancel = UIAlertAction(title: "Cancel", style:.cancel, handler: nil)
            cancel.setValue(UIColor.red, forKey: "titleTextColor")
            tab.addAction(camera)
            tab.addAction(PhotoLibrary)
            tab.addAction(cancel)
            present(tab, animated: true, completion: nil)
        
        
     
        
    }
    
    //MARK: - ADD
    @objc func addOneMore(sender: UIButton){
       // let buttonTag = sender.tag
        DispatchQueue.main.async { [self] in
            let value = arr.count + 1
//            let indexPath = IndexPath(row: buttonTag, section: 0)
//            let cell = self.tableVu.cellForRow(at: indexPath) as! addCell
//
           Helpers.arrOfKeys.append(keyDict(tag: "", key: "", value: ""))
            arr.append("\(value)")
            arr =  arr.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
            debugPrint(Helpers.arrOfKeys)
            tableVu.reloadData()
            scrollToBottom()
        }
    }
    
    //MARK: - REMOVE
    @objc func removeOne(sender: UIButton){
        let buttonTag = sender.tag - 3
        debugPrint(buttonTag,Helpers.arrOfKeys, Helpers.arrOfKeys[buttonTag])
        DispatchQueue.main.async { [self] in
            arr.remove(at: arr.count - 1)
            Helpers.arrOfKeys.remove(at: buttonTag - 1)
            //Helpers.arrOfKeys.append(keyDict(tag: "", key: "", value: ""))
            tableVu.reloadData()
            debugPrint("ghhgfgh",Helpers.arrOfKeys)
        }
    }
    
  
    
    //MARK: - SCROLL TO BUTTOM
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.arr.count + 4, section: 0)
            self.tableVu.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        let indexPath = IndexPath(row: textField.tag, section: 0)
        let cell = self.tableVu.cellForRow(at: indexPath) as! addCell
        Helpers.arrOfKeys[textField.tag - 4] = keyDict.init(tag: "\(textField.tag)", key: cell.keyTf.text!, value: cell.valueTf.text!)

                    print(Helpers.arrOfKeys,textField.tag)
      }
    
    

    @objc func addImages(sender: UIButton){
        let buttonTag = sender.tag - 4
        DispatchQueue.main.async { [self] in
     @StateObject var model = CameraViewModel()
            let actionSheetController = UIAlertController(title: "Please select", message: "", preferredStyle: .actionSheet)

            // Create and add the Cancel action
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                // Just dismiss the action sheet
                self.dismiss(animated: true, completion: nil)
            }
            actionSheetController.addAction(cancelAction)

            // Create and add first option action
            let ascending = UIAlertAction(title: "Capture", style: .default) { action -> Void in
                print(Helpers.detailsOfNED?.name )
                if Helpers.detailsOfNED?.name != nil && Helpers.detailsOfNED?.name != ""{
                ParsalUtilities.check = ""
                let swiftUIView = ContentView(model: model)
                    let hostingController = UIHostingController(rootView: swiftUIView)
               
                hostingController.modalPresentationStyle = .fullScreen
                self.present(hostingController, animated: true)
                }else{
                    showAlert("Add name of Model")
                }
            }
            actionSheetController.addAction(ascending)

            // Create and add a second option action
            let descending = UIAlertAction(title: "Upload", style: .default) { action -> Void in
                ParsalUtilities.check = "true"
            
                
                let swiftUIView = CaptureFoldersView(model: model)
                    let hostingController = UIHostingController(rootView: swiftUIView)
               
                hostingController.modalPresentationStyle = .fullScreen
                self.present(hostingController, animated: true)
               
            }
            actionSheetController.addAction(descending)
            // We need to provide a popover sourceView when using it on iPad
            actionSheetController.popoverPresentationController?.sourceView = sender as? UIView

            // Present the AlertController
            self.present(actionSheetController, animated: true, completion: nil)
        
        
        
        
        }        
    }
    
  
}


//MARK: - IMAGE PICKER EXTENSION
extension ViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imageSelection(tag: Int){
        self.lastSelectedIndex = tag
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.isEditing = true
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.lastSelectedIndex = 0
        let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        let indexPath = IndexPath(row: lastSelectedIndex!, section: 0)
        let cell = self.tableVu.cellForRow(at: indexPath) as! uploadImageCell
        cell.uploadImg.image  = image
        Helpers.thumbnail_image = image
        if show == true{
        tableVu.reloadData()
 
            show = false
        }
 self.dismiss(animated: true)

    }
}


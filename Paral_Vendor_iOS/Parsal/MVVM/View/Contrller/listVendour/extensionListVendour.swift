//
//  extensionListVendour.swift
//  Parsal
//
//  Created by Hussnain Ali on 23/02/2022.
//

import Foundation
import UIKit
import SDWebImage

//MARK: - TABLE VIEW EXTENSION
extension listVendourVc: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vendListObj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableVu.dequeueReusableCell(withIdentifier: "cell") as! vedourCell
        
        let obj = vendListObj
        let vendObj = obj[indexPath.row]
        cell.nameLbl.text = vendObj.vendor_name
        cell.descriptionLbl.text = vendObj.email
        cell.profileImg.sd_setImage(with: URL(string: (vendObj.logo_url!)), placeholderImage: UIImage(named: "img_placeholder"))
        var arrCat = [String]()
        for i in 0..<vendObj.category!.count{
            let obj = vendObj.category![i]
            let aObj = obj.replacingOccurrences(of: "_", with: " ")
            arrCat.append(aObj.capitalized)
            
            
        }
        let cat = arrCat.joined(separator: ",")
        cell.subCatLbl.text = cat
        
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "detailVC") as! detailVC
        let obj = vendListObj
        let vendObj = obj[indexPath.row]
        vc.vendorName = "\(vendObj.vendor_name ?? "")"
        vc.nameSpace = "\(vendObj.namespace ?? "")"
       // Helpers.selectedVenCat = "\(vendObj.category?[0] ?? "")"
        Helpers.nameSpace = "\(vendObj.namespace ?? "")"
        Helpers.pickedCat = "\(vendObj.category?[0] ?? "")"
       
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
}


//MARK: - SEARCHBAR
extension listVendourVc: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if searchBar.text != "" {
            name  = name.filter { $0.contains(searchBar.text!) }
            self.vendListObj.removeAll()
            self.vendListObj = self.nonFilterData.filter {($0.vendor_name!.lowercased().contains(searchBar.text!.lowercased()))}
            tableVu.reloadData()
        }else{
            self.vendListObj.removeAll()
            if let model = self.vendorObj?.data?.vendors_list{
                self.vendListObj = model
            }
            
            tableVu.reloadData()
        }
    }
    //MARK: - ACTICTIONSHEET
    
    func actionSheet(sender: Any){
        // Create the AlertController
        let actionSheetController = UIAlertController(title: "Please select", message: "", preferredStyle: .actionSheet)
        
        // Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            // Just dismiss the action sheet
            self.dismiss(animated: true, completion: nil)
        }
        actionSheetController.addAction(cancelAction)
        
        // Create and add first option action
        let ascending = UIAlertAction(title: "Ascending (Name)", style: .default) { action -> Void in
            self.vendListObj = self.vendListObj.sorted{ $0.vendor_name ?? "" < $1.vendor_name ?? "" }
            
            self.tableVu.reloadData()
        }
        actionSheetController.addAction(ascending)
        
        // Create and add a second option action
        let descending = UIAlertAction(title: "Descending (Name)", style: .default) { action -> Void in
            self.vendListObj = self.vendListObj.sorted{ $0.vendor_name ?? "" > $1.vendor_name ?? "" }
            
            self.tableVu.reloadData()
        }
        actionSheetController.addAction(descending)
        
        
        
        // We need to provide a popover sourceView when using it on iPad
        actionSheetController.popoverPresentationController?.sourceView = sender as? UIView
        
        // Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
}

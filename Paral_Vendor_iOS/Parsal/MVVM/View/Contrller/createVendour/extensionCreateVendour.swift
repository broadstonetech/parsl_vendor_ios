//
//  extensionCreateVendour.swift
//  Parsal
//
//  Created by Hussnain Ali on 23/02/2022.
//

import Foundation
import UIKit
import FlagPhoneNumber
//MARK: - TABLE VIEW EXTENSION

extension createVendourVC: UITableViewDelegate,UITableViewDataSource, SelectionDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = newVendourTableView.dequeueReusableCell(withIdentifier: "uploadCell", for: indexPath) as! uploadImageCell
            cell.uploadBtn.tag = indexPath.row
            cell.uploadBtn.addTarget(self, action: #selector(imageUpload(sender:)), for: .touchUpInside)
            
            return cell
        }else if indexPath.row == 1{
            let cell = newVendourTableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath) as! infoCell
            cell.emailTf.tag = indexPath.row
            cell.descriptionTV.tag = indexPath.row
            cell.nameTf.tag = indexPath.row
            cell.phoneTf.tag = indexPath.row
            
            
            return cell
        }else if indexPath.row == 2{
            let cell = newVendourTableView.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath) as! locationCell
            cell.cityTf.tag = indexPath.row
            cell.stateTf.tag = indexPath.row
            cell.zipCodeTf.tag = indexPath.row
            cell.countryTf.tag = indexPath.row
            
          //  cell.countryTf.addTarget(self, action: #selector(myTargetFunction), for: .touchUpInside)
            return cell
        }else{
            let cell = newVendourTableView.dequeueReusableCell(withIdentifier: "CreateVenCatTVC", for: indexPath) as! CreateVenCatTVC
            cell.catBtn.addTarget(self, action: #selector(catPicker(sender:)), for: .touchUpInside)

            return cell
            
        }
        
    }
    
    
    //MARK: - PICKER BUTTON
    
    @objc func myTargetFunction(textField: UITextField) {
        
    }
    @objc func catPicker(sender: UIButton){
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "pickerVC") as! pickerVC
        vc.type = "cat"
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
        vc.selectionDelegate = self
        
    }
    @objc func subCatPicker(sender: UIButton){
        let vc = storyboard?.instantiateViewController(withIdentifier: "pickerVC") as! pickerVC
        vc.type = "subCat"
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
        vc.selectionDelegate = self
    }
    
    func tabForData(type:String) {
        print("======")
        let indexPath = IndexPath(row: 3, section: 0)
        let cell = self.newVendourTableView.cellForRow(at: indexPath) as! CreateVenCatTVC
        if type == "cat" {
            
            cell.catBtn.setTitle(Helpers.cat, for: .normal)
            cell.catBtn.setTitleColor(.black, for: .normal)
            
           
        }
        
    }
    @objc func imageUpload(sender: UIButton){
        
        let tab = UIAlertController(title:nil, message: nil, preferredStyle:
                                            .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default) { (alert:UIAlertAction) in
            self.imagePicker.sourceType = .camera
            self.imagePicker.delegate = self
            self.imagePicker.isEditing = true
            
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        let PhotoLibrary = UIAlertAction(title: "Photo Library", style: .default) { (alert:UIAlertAction) in
            
            self.imageSelection(tag: sender.tag)
            debugPrint(sender.tag)
            
        }
        let cancel = UIAlertAction(title: "Cancel", style:.cancel, handler: nil)
        cancel.setValue(UIColor.red, forKey: "titleTextColor")
        tab.addAction(camera)
        tab.addAction(PhotoLibrary)
        tab.addAction(cancel)
        present(tab, animated: true, completion: nil)
        
    }
    
    
}

//MARK: - IMAGE PICKER EXTENSION
extension createVendourVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imageSelection(tag: Int){
        self.lastSelectedIndex = tag
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.isEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        lastSelectedIndex = 0
        let indexPath = IndexPath(row: lastSelectedIndex!, section: 0)
        let cell = self.newVendourTableView.cellForRow(at: indexPath) as! uploadImageCell
        cell.uploadImg.image  = image
        self.logo_file = image
        
        debugPrint("logo",logo_file)
        self.dismiss(animated: true)
        
    }
}


extension createVendourVC: FPNTextFieldDelegate {
    
    /// The place to present/push the listController if you choosen displayMode = .list
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        
        present(navigationViewController, animated: true, completion: nil)
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
    }
    
    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            // Do something...
            //         textField.getFormattedPhoneNumber(format: .E164),           // Output "+33600000001"
            //         textField.getFormattedPhoneNumber(format: .International),  // Output "+33 6 00 00 00 01"
            //         textField.getFormattedPhoneNumber(format: .National),       // Output "06 00 00 00 01"
            //         textField.getFormattedPhoneNumber(format: .RFC3966),        // Output "tel:+33-6-00-00-00-01"
            //         textField.getRawPhoneNumber()                               // Output "600000001"
        } else {
            // Do something...
        }
    }
}

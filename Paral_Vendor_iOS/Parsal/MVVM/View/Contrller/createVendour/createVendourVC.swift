//
//  createVendourVC.swift
//  Parsal
//
//  Created by Hussnain Ali on 23/02/2022.
//

import UIKit
import SwiftMessages
import SwiftMessages
import FlagPhoneNumber

class createVendourVC: BaseVC {
    
    //MARK: -  Outlets
    @IBOutlet weak var newVendourTableView: UITableView!
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    
    //MARK: -  Variables
    let imagePicker = UIImagePickerController()
    var lastSelectedIndex: Int?
    var vendor_name = ""
    var phone = ""
    var website = ""
    var email = ""
    var category = ""
    var sub_category = ""
    var description_ = ""
    var slogan = ""
    var country_ = ""
    var state_ = ""
    var city_ = ""
    var zipCode_ = ""
    var logo_file: UIImage!
    var dialCode_ = ""
    
    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let rightButton = UIBarButtonItem(title: "Save", style: UIBarButtonItem.Style.plain, target: self, action:  #selector(save(sender:)))
        Helpers.detailsOfNED = nil
        Helpers.detailsOfLocation = nil
        Helpers.cat = "Choose Category"
        Helpers.subCat = "Choose Sub Category"
        
        self.navigationItem.rightBarButtonItem = rightButton
        self.navigationItem.title = "Create Vendour"
        newVendourTableView.delegate = self
        newVendourTableView.dataSource = self
    }
    
    //MARK:  - DISSMISS KEYBOAD
   
    
    //MARK: - Functions
    @objc func save(sender: UIBarButtonItem) {
        self.dismissMyKeyboard()
        
        let infoPer = Helpers.detailsOfNED
        let infoLoc = Helpers.detailsOfLocation
        self.vendor_name =  infoPer?.name ?? ""
        self.phone = infoPer?.phone ?? ""
        self.email = infoPer?.email ?? ""
        self.description_ = infoPer?.discription ?? ""
        self.country_ = infoLoc?.country ?? ""
        self.state_ = infoLoc?.state ?? ""
        self.city_ = infoLoc?.city ?? ""
        self.zipCode_ = infoLoc?.zipCode ?? ""
        self.category = Helpers.pickedCat
        self.sub_category = ""
        self.dialCode_ = Helpers.dailCode ?? ""
        
    
        
        
        if self.logo_file == nil{
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Please select an image", font: .systemFont(ofSize: 14.0))
            return
        }
        
        if ParsalUtilities.sharedInstance.isEmpty(self.vendor_name){
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Please write vendor name.", font: .systemFont(ofSize: 14.0))
            return
        }
        
        if ParsalUtilities.sharedInstance.isEmpty(self.description_){
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Please write Description.", font: .systemFont(ofSize: 14.0))
            return
        }
        
        if ParsalUtilities.sharedInstance.isEmpty(self.email){
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Please write email.", font: .systemFont(ofSize: 14.0))
            return
        }
        
        if !ParsalUtilities.sharedInstance.isEmail(self.email){
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Invalid Email.", font: .systemFont(ofSize: 14.0))
            return
        }
        
        if ParsalUtilities.sharedInstance.isEmpty(self.phone){
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Please write phone number.", font: .systemFont(ofSize: 14.0))
            return
        }
        if !ParsalUtilities.sharedInstance.isValidPhone  {
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Invalid phone number", font: .systemFont(ofSize: 14.0))
            return
        }
        
        if ParsalUtilities.sharedInstance.isEmpty(self.country_){
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Please select country.", font: .systemFont(ofSize: 14.0))
            return
        }
        if ParsalUtilities.sharedInstance.isEmpty(self.state_){
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Please select state.", font: .systemFont(ofSize: 14.0))
            return
        }
        if ParsalUtilities.sharedInstance.isEmpty(self.city_){
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Please select city.", font: .systemFont(ofSize: 14.0))
            return
        }
        
        if Helpers.cat == "Choose Category"{
            ParsalUtilities.sharedInstance.vibrate(with: .weak)
            self.presentToast(message: "Please select category.", font: .systemFont(ofSize: 14.0))
            return
        }
        
       
        
        let trimedNum = self.phone.replacingOccurrences(of: " ", with: "")
        self.phone = self.dialCode_ + trimedNum
        
        debugPrint("phonNum :",self.phone)
        
        var parameters : [String: AnyObject] = [:]
        parameters["vendor_name"] = "\(infoPer?.name ?? "")"  as AnyObject
        parameters["phone"] = "\(infoPer?.phone ?? "")"  as AnyObject
        parameters["website"] = ""  as AnyObject
        parameters["email"] = "\(infoPer?.email ?? "")"  as AnyObject
        parameters["category"] = ""  as AnyObject
        parameters["sub_category"] = ""  as AnyObject
        parameters["slogan"] = ""  as AnyObject
        parameters["description"] = "\(infoPer?.discription ?? "")"  as AnyObject
        //parameters["logo_file"] = jpgData  as AnyObject
        
        // self.getPortfolioList(useCaseType: parameters, isNavigatingBack: true)
        
        self.addVendorAPI()
    }
    
    
    //MARK: - Api Function
    
    func addVendorAPI() {
        if ParsalUtilities.sharedInstance.connected() == false{
            
            return
        }
        let timeInterval = NSDate().timeIntervalSince1970
        ApiHandler.sharedInstance.AddNewVendor(vendor_name: self.vendor_name, phone: self.phone, website: self.website, email: self.email, category: self.category, sub_category: self.sub_category, description: self.description_, slogan: "",country_name: self.country_,city: self.city_,state: self.state_,zip_code: self.zipCode_, timestamp: "\(timeInterval)", logo_file: self.logo_file!, controller: self) { result, resp in
            
            
            if result {
                debugPrint("checkController",resp)
                
                if resp?.value(forKey: "message") as! String == "Success" {
                    debugPrint("Vendor Added")
                    Helpers.isAddedNewThing = true
                    self.navigationController?.popViewController(animated: true)
                    
                }else if resp?.value(forKey: "message") as! String == "Vendor alreday exists." {
                    debugPrint("Vendor NotAdded")
                    let alert = UIAlertController(title: "Sorry", message: "The Name is already taken.", preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
        }
    }
    
    
    
    
    
}

//
//  pickerVC.swift
//  Parsal
//
//  Created by Hussnain Ali on 22/02/2022.
//

import UIKit

protocol SelectionDelegate {
    func tabForData(type:String)
}

class pickerVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate   {
    
    //MARK:- WITH PROTOCOL & DELEGATE
    var selectionDelegate: SelectionDelegate!
    
    var pickerView: UIPickerView!
    let catData = ["Decor","Electronics","Architecture","Fashion","Food","Special Occasions"]
    let subCatData = ["0","8","9"]
    
    let dictSubCat  = ["decor":["Livingroom Design", "Kitchen Design", "Landscape Design", "Washroom Design","others"],
                      "electronics":["Mobile", "LCD TV", "Laptop","others"],
                      "architecture":["Livingroom Design", "Kitchen Design", "Landscape Design", "Washroom Design","others"],
                      "fashion":["Male", "Female", "Kids","others"],
                      "food":["Burger", "Pizza", "Pasta","Sandwich","others"],
                      "special_occasions":["Christmas", "Halloween","Mothers Day","others"]]
    
    var pickerData: [String] = [String]()
    
    var type : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView = UIPickerView(frame: CGRect(x: 10, y: 50, width: 250, height: 150))
        pickerView.delegate = self
        pickerView.dataSource = self
        
        // This is where you can set your min/max values
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showAlert()
    }
    
    
    func showAlert() {
        let ac = UIAlertController(title: "Picker", message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        ac.view.addSubview(pickerView)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            let selectedRow = self.pickerView.selectedRow(inComponent: 0)
            if self.type == "cat"{
                
                let pickerValue = self.catData[selectedRow]
                let finaValue = pickerValue.replacingOccurrences(of: " ", with: "_")
               // let aVal  = self.dictCatData[pickerValue]
              //  print(aVal ?? "notHapp")
                print("Picker value: \(finaValue.lowercased()) was selected")
                Helpers.pickedCat = finaValue.lowercased()
                
                Helpers.cat = pickerValue
               // Helpers.subCat = "Choose Sub Category"
                self.selectionDelegate.tabForData(type: self.type)
                
            }else{
                let pickerValue = self.dictSubCat["\(Helpers.pickedCat)"]![selectedRow]
                let finalVal = pickerValue.replacingOccurrences(of: " ", with: "_")
                
            
                print("Picker value: \(finalVal.lowercased()) was selected")
                Helpers.subCat = pickerValue
                self.selectionDelegate.tabForData(type: self.type)
            }
            
            self.dismiss(animated: true, completion: nil)
        }))
        
        ac.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { _ in
            
            self.dismiss(animated: true, completion: nil)
        }))
        present(ac, animated: true)
    }
    
    
    // MARK: UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        print(type)
        if type == "cat"{
            return catData.count
        }else{
            return dictSubCat["\(Helpers.pickedCat)"]?.count ?? 0
        }
        
    }
    
    // MARK: UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
        if type == "cat"{
            let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 50, height: 30))
            
            let myImageView = UIImageView(frame: CGRect(x: 170, y: 0, width: 25, height: 25))
            
            let label = UILabel(frame: CGRect(x: 30, y: -6, width: pickerView.bounds.width - 90, height: 40 ))
            //
            //                myImageView.image = images[row]
            label.font = UIFont(name: "Helvetica Neue", size: 17)
            label.textColor = UIColor.black
            label.text = catData[row]
            myView.addSubview(label)
            myView.addSubview(myImageView)
            
            return myView
        }else{
            let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 50, height: 30))
            
            let myImageView = UIImageView(frame: CGRect(x: 170, y: 0, width: 25, height: 25))
            
            let label = UILabel(frame: CGRect(x: 30, y: -6, width: pickerView.bounds.width - 90, height: 40 ))
            
            //                    myImageView.image = images[row]
            label.font = UIFont(name: "Helvetica Neue", size: 17)
            label.textColor = UIColor.black
            label.text = self.dictSubCat["\(Helpers.pickedCat)"]![row]
            myView.addSubview(label)
            myView.addSubview(myImageView)
            
            return myView
            
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        // do something with selected row
    }
    
}

//
//  vedourCell.swift
//  Parsal
//
//  Created by Hussnain Ali on 22/02/2022.
//

import UIKit

class vedourCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var subCatLbl: UILabel!
    @IBOutlet weak var activeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowRadius = 4
        bgView.layer.shadowOpacity = 2
        bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bgView.layer.cornerRadius = 5
        profileImg.layer.cornerRadius = profileImg.frame.height/2
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

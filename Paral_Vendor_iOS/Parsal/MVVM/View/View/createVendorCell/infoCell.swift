//
//  infoCell.swift
//  Parsal
//
//  Created by Hussnain Ali on 23/02/2022.
//

import UIKit
import FlagPhoneNumber

class infoCell: UITableViewCell, UITextFieldDelegate, UITextViewDelegate {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var phoneTf: FPNTextField!
    
    var phoneNumIsValid = false
    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowRadius = 4
        bgView.layer.shadowOpacity = 2
        bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bgView.layer.cornerRadius = 5
        
        self.descriptionTV.layer.cornerRadius = 4
        self.descriptionTV.layer.borderColor = UIColor.systemGray4.cgColor
        self.descriptionTV.layer.borderWidth = 0.6
        nameTf.delegate = self
        descriptionTV.delegate = self
        emailTf.delegate = self
        phoneTf.delegate = self
        
        phoneTf.displayMode = .list
        
        listController.setup(repository: phoneTf.countryRepository )
        listController.didSelect = { [weak self] country in
            
            self?.phoneTf.setFlag(countryCode: country.code)
            Helpers.dailCode = country.phoneCode
            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        
        Helpers.detailsOfNED = infoDict.init(tag: "\(textField.tag)", name: nameTf.text!, email: emailTf.text!, discription: descriptionTV.text!, phone: phoneTf.text!)
        
    }
    func textViewDidChangeSelection(_ textView: UITextView) {
        Helpers.detailsOfNED = infoDict.init(tag: "\(textView.tag)", name: nameTf.text!, email: emailTf.text!, discription: descriptionTV.text!, phone: phoneTf.text!)
    }
    
    
    
}
extension infoCell: FPNTextFieldDelegate {
    
    /// The place to present/push the listController if you choosen displayMode = .list
    func fpnDisplayCountryList() {
        
        let navigationViewController = UINavigationController(rootViewController: listController)
        listController.title = "Countries"
        
        
        self.window?.rootViewController?.present(navigationViewController, animated: true, completion: nil)
        //present(navigationViewController, animated: true, completion: nil)
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        
        
        debugPrint(name, dialCode, code) // Output "France", "+33", "FR"
    }
    
    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            // Do something...
            //         textField.getFormattedPhoneNumber(format: .E164),           // Output "+33600000001"
            //         textField.getFormattedPhoneNumber(format: .International),  // Output "+33 6 00 00 00 01"
            //         textField.getFormattedPhoneNumber(format: .National),       // Output "06 00 00 00 01"
            //         textField.getFormattedPhoneNumber(format: .RFC3966),        // Output "tel:+33-6-00-00-00-01"
            //         textField.getRawPhoneNumber()                               // Output "600000001"
            self.phoneNumIsValid = true
            ParsalUtilities.sharedInstance.isValidPhone = true
            debugPrint("Phone Number::",textField.text!)
        } else {
            ParsalUtilities.sharedInstance.isValidPhone = false
            self.phoneNumIsValid = false
        }
    }
}

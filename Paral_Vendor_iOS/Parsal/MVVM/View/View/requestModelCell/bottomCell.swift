//
//  bottomCell.swift
//  Parsal
//
//  Created by Hussnain Ali on 23/02/2022.
//

import UIKit


class bottomCell: UITableViewCell {
    @IBOutlet weak var addImages: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var LblFolder: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowRadius = 4
        bgView.layer.shadowOpacity = 2
        bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bgView.layer.cornerRadius = 5
        addImages.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func openCaptureSession(_ sender: Any) {
      
        
    
}
    
}

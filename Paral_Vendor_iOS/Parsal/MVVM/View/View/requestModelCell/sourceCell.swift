//
//  sourceCell.swift
//  Parsal
//
//  Created by Hussnain Ali on 21/02/2022.
//

import UIKit

class sourceCell: UITableViewCell, UITextFieldDelegate, UITextViewDelegate {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var sourceTf: UITextField!
    @IBOutlet weak var priceTf: UITextField!
    @IBOutlet weak var firstBlankTf: UITextField!
    @IBOutlet weak var secondBlankTf: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        priceTf.keyboardType = .numberPad
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowRadius = 4
        bgView.layer.shadowOpacity = 2
        bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bgView.layer.cornerRadius = 5
        sourceTf.delegate = self
        priceTf.delegate = self
        firstBlankTf.delegate = self
        secondBlankTf.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {

    Helpers.detailsOfSource =  sourceDict.init(tag: "\(textField.tag)", source: sourceTf.text!, price: priceTf.text!, first: firstBlankTf.text!, second: secondBlankTf.text!)
      }
}

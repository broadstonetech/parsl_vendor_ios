//
//  twoFieldsCell.swift
//  Parsal
//
//  Created by Hussnain Ali on 18/02/2022.
//

import UIKit

class twoFieldsCell: UITableViewCell, UITextFieldDelegate, UITextViewDelegate {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameTf: UITextField!
//    @IBOutlet weak var DescriptionTf: UITextField!
//    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var textView: UITextView!
    
   // var infoDict : [String:String] = [:]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameTf.keyboardType = .namePhonePad
        self.textView.layer.cornerRadius = 4
        self.textView.layer.borderColor = UIColor.systemGray4.cgColor
        self.textView.layer.borderWidth = 0.6
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowRadius = 4
        bgView.layer.shadowOpacity = 2
        bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bgView.layer.cornerRadius = 5
        nameTf.delegate = self
        textView.delegate = self
        textView.layer.borderWidth = 0.3
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.layer.cornerRadius = 3
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
    
    Helpers.detailsOfNED =  infoDict.init(tag: "\(textField.tag)", name: nameTf.text!)
    
       }
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.textColor = .black
        Helpers.detailsOfNED = infoDict.init(tag: "\(textView.tag)", name: nameTf.text!, email: "", discription: textView.text!, phone: "")

        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = .black
    }
}

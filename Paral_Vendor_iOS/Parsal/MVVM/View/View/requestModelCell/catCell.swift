//
//  catCell.swift
//  Parsal
//
//  Created by Hussnain Ali on 22/02/2022.
//

import UIKit

class catCell: UITableViewCell {
    @IBOutlet weak var catBtn: UIButton!
    @IBOutlet weak var subCatBtn: UIButton!
    @IBOutlet weak var bgView: UIView!
    var pickerView: UIPickerView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowRadius = 4
        bgView.layer.shadowOpacity = 2
        bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bgView.layer.cornerRadius = 5
        catBtn.layer.borderWidth = 0.3
        catBtn.layer.borderColor = UIColor.lightGray.cgColor
        catBtn.layer.cornerRadius = 3
        subCatBtn.layer.borderWidth = 0.3
        subCatBtn.layer.borderColor = UIColor.lightGray.cgColor
        subCatBtn.layer.cornerRadius = 3

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }


}

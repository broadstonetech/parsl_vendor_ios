//
//  locationCell.swift
//  Parsal
//
//  Created by Hussnain Ali on 21/02/2022.
//

import UIKit
import FlagPhoneNumber
import IQKeyboardManagerSwift


class locationCell: UITableViewCell, UITextFieldDelegate, UITextViewDelegate {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cityTf: UITextField!
    @IBOutlet weak var stateTf: UITextField!
    @IBOutlet weak var zipCodeTf: UITextField!
    @IBOutlet weak var countryTf: UITextField!
    
    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    let phoneNumberTextField = FPNTextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        zipCodeTf.keyboardType = .numberPad
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowRadius = 4
        bgView.layer.shadowOpacity = 2
        bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bgView.layer.cornerRadius = 5
        cityTf.delegate = self
        stateTf.delegate = self
        zipCodeTf.delegate = self
        countryTf.delegate = self
        phoneNumberTextField.delegate = self
        phoneNumberTextField.displayMode = .list
        phoneNumberTextField.flagButton.isHidden = true
        
        listController.setup(repository: phoneNumberTextField.countryRepository )
        listController.didSelect = { [weak self] country in
            self?.countryTf.text = country.name
            Helpers.detailsOfLocation =  locationDict.init(tag: "1", city: (self?.cityTf.text)!, state: (self?.stateTf.text)!, zipCode: (self?.zipCodeTf.text)!, country: country.name)
            self?.window?.rootViewController?.dismiss(animated: true)
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func countrySelect(_ sender: UITextField) {
        
        self.fpnDisplayCountryList()
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        
        
        Helpers.detailsOfLocation =  locationDict.init(tag: "\(textField.tag)", city: cityTf.text!, state: stateTf.text!, zipCode: zipCodeTf.text!, country: countryTf.text!)
        
    }
    
   
}
extension locationCell: FPNTextFieldDelegate {
    
    /// The place to present/push the listController if you choosen displayMode = .list
    func fpnDisplayCountryList() {
        self.countryTf.resignFirstResponder()
        let navigationViewController = UINavigationController(rootViewController: listController)
        listController.title = "Countries"
        
        
        self.window?.rootViewController?.present(navigationViewController, animated: true, completion: nil)
        // present(navigationViewController, animated: true, completion: nil)
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        
        self.countryTf.text = name
        debugPrint(name, dialCode, code) // Output "France", "+33", "FR"
    }
    
    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            // Do something...
            //         textField.getFormattedPhoneNumber(format: .E164),           // Output "+33600000001"
            //         textField.getFormattedPhoneNumber(format: .International),  // Output "+33 6 00 00 00 01"
            //         textField.getFormattedPhoneNumber(format: .National),       // Output "06 00 00 00 01"
            //         textField.getFormattedPhoneNumber(format: .RFC3966),        // Output "tel:+33-6-00-00-00-01"
            //         textField.getRawPhoneNumber()                               // Output "600000001"
        } else {
            // Do something...
        }
    }
}

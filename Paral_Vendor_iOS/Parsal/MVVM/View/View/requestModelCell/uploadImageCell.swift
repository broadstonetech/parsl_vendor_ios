//
//  uploadImageCell.swift
//  Parsal
//
//  Created by Hussnain Ali on 18/02/2022.
//

import UIKit

class uploadImageCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var uploadImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowRadius = 4
        bgView.layer.shadowOpacity = 2
        bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bgView.layer.cornerRadius = 5
        uploadImg.layer.shadowColor = UIColor.lightGray.cgColor
        uploadImg.layer.shadowRadius = 4
        uploadImg.layer.shadowOpacity = 2
        uploadImg.layer.shadowOffset = CGSize(width: 0, height: 0)
        uploadImg.layer.cornerRadius = 5
        uploadImg.layer.cornerRadius = uploadImg.frame.height/2
        uploadImg.contentMode = .scaleAspectFill
        uploadBtn.layer.cornerRadius = 4
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
       
    }
    
}

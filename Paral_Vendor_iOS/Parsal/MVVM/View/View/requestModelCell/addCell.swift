//
//  addCell.swift
//  Parsal
//
//  Created by Hussnain Ali on 18/02/2022.
//

import UIKit

class addCell: UITableViewCell, UITextFieldDelegate, UITextViewDelegate{
    
    @IBOutlet weak var keyTf: UITextField!
    @IBOutlet weak var valueTf: UITextField!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var LblHeading: UILabel!
    var value : String?{
        willSet{
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        keyTf.keyboardType = .namePhonePad
        valueTf.keyboardType = .namePhonePad
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowRadius = 4
        bgView.layer.shadowOpacity = 2
        bgView.layer.shadowOffset = CGSize(width: 0, height: 0)
        bgView.layer.cornerRadius = 5
        removeBtn.isHidden = true
        addBtn.layer.cornerRadius = 4
}
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
   

}

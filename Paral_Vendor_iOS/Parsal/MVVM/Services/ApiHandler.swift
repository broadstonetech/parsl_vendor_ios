//
//  ApiHandler.swift
//  Parsal
//
//  Created by Naqash Ali on 02/03/2022.
//
import Foundation
import UIKit
import Alamofire
import ActivityIndicatorView

var BASE_URL = "https://api.parsl.io/"

let API_BASE_URL = BASE_URL

let baseVC = BaseVC()
private let SharedInstance = ApiHandler()


enum Endpoint : String {
    
    case add_new_vendor        =  "add/new/vendor"
    case get_new_vendor        =  "get/new/vendor"
    case get_vendor_info       =  "get/vendor/info"
    case get_vendor_products   =  "get/vendor/products"
}

class ApiHandler:NSObject{
    class var sharedInstance : ApiHandler {
        return SharedInstance
    }
    
    override init() {
        
    }
    
    //MARK: Add New Vendor
    func AddNewVendor(vendor_name:String,phone:String,website:String,email:String,category:String,sub_category:String,description:String,slogan:String,country_name:String,city:String,state:String,zip_code:String,timestamp:String, logo_file: UIImage,controller:UIViewController,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
   
        var parameters = [String : Any]()
        parameters = [
            "vendor_name" : vendor_name,
            "phone":   phone,
            "website":  website,
            "email":  email,
            "category":  category,
            "sub_category":  sub_category,
            "description":  description,
         ]
        let finalUrl =   BASE_URL + Endpoint.add_new_vendor.rawValue
        
        debugPrint(finalUrl)
        debugPrint(parameters)
      
        ParsalUtilities.sharedInstance.showProgressHUD()
        let imgData = logo_file.jpegData(compressionQuality: 0.5)!
        Alamofire.upload(
            multipartFormData: { multipart in
                
                multipart.append(Data("\(vendor_name)".utf8), withName :"vendor_name")
                multipart.append(Data("\(phone)".utf8), withName :"phone")
                multipart.append(Data("\(website)".utf8), withName :"website")
                multipart.append(Data("\(email)".utf8), withName :"email")
                multipart.append(Data("\(category)".utf8), withName :"category")
                multipart.append(Data("\(sub_category)".utf8), withName :"sub_category")
                multipart.append(Data("\(description)".utf8), withName :"description")
                multipart.append(Data("\(slogan)".utf8), withName :"slogan")
                multipart.append(Data("\(country_name)".utf8), withName :"country_name")
                multipart.append(Data("\(city)".utf8), withName :"city")
                multipart.append(Data("\(state)".utf8), withName :"state")
                multipart.append(Data("\(zip_code)".utf8), withName :"zip_code")
                multipart.append(Data("\(timestamp)".utf8), withName :"timestamp")
                multipart.append(imgData, withName: "logo_file", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                
            }, to: finalUrl,method: .post, headers: nil) { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { answer in
                        let code = answer.response?.statusCode
                        let json =  answer.value
                        
                        do {
                            let dict = json as? NSDictionary
                            completionHandler(true, dict)
                           
                        }
                        if code == 200{
                            
                                ParsalUtilities.sharedInstance.dismissProgressHUD()
                        }else if code == 202{
                            DispatchQueue.main.async {
                                ParsalUtilities.sharedInstance.dismissProgressHUD()
                             
                            }
                        }else if code == 503{
                            DispatchQueue.main.async {
                                ParsalUtilities.sharedInstance.dismissProgressHUD()
                                ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ERROR_TEXT_503, delegate: controller)
                            }
                        }else if code == 409{
                            DispatchQueue.main.async {
                                ParsalUtilities.sharedInstance.dismissProgressHUD()
                                ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ERROR_TEXT_409, delegate: controller)
                            }
                        }else {
                            DispatchQueue.main.async {
                                ParsalUtilities.sharedInstance.dismissProgressHUD()
                                ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ErrorText, delegate: controller)
                            }
                        }
                      
                        //ParsalUtilities.sharedInstance.dismissProgressHUD()
                       // debugPrint("checkkApi",response.result.value!)
                    }
                    
                    
                    break
                case .failure(let encodingError):
                    ParsalUtilities.sharedInstance.dismissProgressHUD()
                    ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ErrorText, delegate: controller)
                    debugPrint("checkkApi1",encodingError)
                    break
                }
                
                
            }
     }
    //MARK: Get All Vendor
    func getAllVendors(controller:UIViewController,completionHandler:@escaping( _ result:Bool, _ vendorListObj:VendorListModel?)->Void){
        //        let headers: HTTPHeaders = [
        //            "Api-Key":API_KEY
        //        ]
        var parameters = [String : Any]()
        ParsalUtilities.sharedInstance.showProgressHUD()
        
        parameters = [
            "":""
        ]
        
        let finalUrl = BASE_URL + Endpoint.get_new_vendor.rawValue
        
        debugPrint(finalUrl)
        debugPrint(parameters)
        
        
        Alamofire.request(finalUrl, method: .post, parameters: nil, encoding: JSONEncoding.default).responseJSON { response in
            
            
           
            switch response.result {
            case .success(_):
                
                let  vendorListModel = try? JSONDecoder().decode(VendorListModel.self, from: response.data!)
            
                if response.value != nil {
                    
                    let code =  response.response?.statusCode
                    
                    if code == 200{
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            do {
                                completionHandler(true, vendorListModel)
                            }
                        }
                    }else if code == 503{
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ERROR_TEXT_503, delegate: controller)
                        }
                    }else if code == 404{
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ERROR_TEXT_409, delegate: controller)
                        }
                    }else {
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ErrorText, delegate: controller)
                        }
                    }
                    
                    
                    debugPrint("statuss:",code!)
                   
                }
                break
                
            case .failure(let error):
                debugPrint("err",error)
                DispatchQueue.main.async {
                    ParsalUtilities.sharedInstance.dismissProgressHUD()
                    ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ErrorText, delegate: controller)
                }
                break
            }
        }
    }
    
    
    //MARK: Get Vendor All Products
    func getVendorAllProduct(namespace:String,controller:UIViewController,completionHandler:@escaping( _ result:Bool, _ vendorProductsObj:GetVendorProductModel?)->Void){
        //        let headers: HTTPHeaders = [
        //            "Api-Key":API_KEY
        //        ]
        var parameters = [String : Any]()
        
        
        ParsalUtilities.sharedInstance.showProgressHUD()
        
        parameters = [
            "namespace":namespace
        ]
        
        let finalUrl = BASE_URL + Endpoint.get_vendor_products.rawValue
        
        debugPrint(finalUrl)
        debugPrint(parameters)
        
        
        Alamofire.request(finalUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            
            ParsalUtilities.sharedInstance.dismissProgressHUD()
            switch response.result {
            case .success(_):
                let  getVendorProductModel = try? JSONDecoder().decode(GetVendorProductModel.self, from: response.data!)
                
                
                if response.value != nil{
                    
                    let code =  response.response?.statusCode
                    
                    if code == 200{
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            do {
                                
                                completionHandler(true, getVendorProductModel)
                            }
                            
                        }
                    }else if code == 503{
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ERROR_TEXT_503, delegate: controller)
                        }
                    }else if code == 409{
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ERROR_TEXT_409, delegate: controller)
                        }
                    }else {
                        DispatchQueue.main.async {
                            ParsalUtilities.sharedInstance.dismissProgressHUD()
                            ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ErrorText, delegate: controller)
                        }
                    }
                    
                    
                    debugPrint("statuss:",code!)
                    
                    
                }
                break
                
            case .failure(let error):
                debugPrint(error)
                DispatchQueue.main.async {
                    ParsalUtilities.sharedInstance.dismissProgressHUD()
                    ParsalUtilities.sharedInstance.displayAlert(title: "Parsal Vendor", messageText: ConstantStrings.ErrorText, delegate: controller)
                }
                break
            }
        }
    }
    
    
    
}




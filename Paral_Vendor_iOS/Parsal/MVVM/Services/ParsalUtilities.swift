//
//  ParsalUtilities.swift
//  Parsal
//
//  Created by Hussnain Ali on 24/02/2022.
//

import Foundation
import CryptoKit
import CommonCrypto
import KRProgressHUD
import AudioUnit
import Connectivity


class ParsalUtilities: NSObject{
    
    static var sharedInstance = ParsalUtilities()
    static var check  = "false"
    static var comeFrom = ""
    var isValidPhone = false
    

    
    enum Vibration : UInt {
        case weak = 1519
        case threeBooms = 1107
    }

    func saveSizeOfImages(_ size: String){
            UserDefaults.standard.set(size, forKey: "ImageSize")
            UserDefaults.standard.synchronize()
        }
        func saveDirectoryPath(_ path: String){
            UserDefaults.standard.set(path, forKey: "DirectoryPath")
            UserDefaults.standard.synchronize()
        }
        func saveIfImagesUploadingDone(_ imagesStatus: Bool){
            UserDefaults.standard.set(imagesStatus, forKey: "ImagesUploadingStatus")
            UserDefaults.standard.synchronize()
        }
        func saveDepthCameraAvailable(_ camera: String) {
            UserDefaults.standard.set(camera, forKey: "DepthCameraAvailable")
            UserDefaults.standard.synchronize()
        }
        func saveLastCount (_ count: String) {
            UserDefaults.standard.set(count, forKey: "LastCount")
            UserDefaults.standard.synchronize()
        }
        func saveFolderName (_ folderName: String) {
            UserDefaults.standard.set(folderName, forKey: "FolderName")
            UserDefaults.standard.synchronize()
        }
        func getFolderName () -> String {
            let folderName = (UserDefaults.standard.value(forKey: "FolderName") as? String) ?? ""
            return folderName
        }
        func getLastCount () -> String {
            let count = (UserDefaults.standard.value(forKey: "LastCount") as? String) ?? ""
            return count
        }
        func getIfDepthCameraAvailable () -> String {
            let camera = (UserDefaults.standard.value(forKey: "DepthCameraAvailable") as? String) ?? ""
            return camera
        }
        func getifImagesUPloaded() -> Bool {
            let status = (UserDefaults.standard.value(forKey: "ImagesUploadingStatus") as? Bool) ?? false
            return status
        }
        func getSizeOfImages() -> String {
            let size = (UserDefaults.standard.value(forKey: "ImageSize") as? String) ?? ""
            return size
        }
        func getDirectoryPath() -> String {
            let path = (UserDefaults.standard.value(forKey: "DirectoryPath") as? String) ?? ""
            return path
        }
    
    //MARK:- Validation Email
    func isEmail(_ email:String  ) -> Bool
    {
        let strEmailMatchstring = "\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b"
        let regExPredicate = NSPredicate(format: "SELF MATCHES %@", strEmailMatchstring)
        if(!isEmpty(email as String?) && regExPredicate.evaluate(with: email))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //MARK:- Validation Empty
      func isEmpty(_ thing : String? )->Bool {
          
          if (thing?.count == 0) {
              return true
          }
          return false;
      }
      

    
    //MARK: Vibrate Alert
   
      func vibrate(with vibration : Vibration) {
          AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
      }

     

    
    //MARK: KRProgressHUD
        
        func showProgressHUD(){
            KRProgressHUD.show()
            
        }
        
        func dismissProgressHUD(){
            
            KRProgressHUD.set(activityIndicatorViewColors:[UIColor.black])
            KRProgressHUD.dismiss()
            
        }

    //MARK: Check Internet
    func connected() -> Bool
    {
        let reachibility = Reachability.forInternetConnection()
        let networkStatus = reachibility?.currentReachabilityStatus()
        
        return networkStatus != NotReachable
        
    }
    
    //MARK: Display Alert
    func displayAlert(title titleTxt:String, messageText msg:String, delegate controller:UIViewController) ->()
    {
        let alertController = UIAlertController(title: titleTxt, message: msg, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { UIAlertAction in
            controller.navigationController?.dismiss(animated: true)
            controller.navigationController?.popViewController(animated: true)
        }))
        controller.present(alertController, animated: true, completion: nil)
        //alertController.view.tintColor = UIColor(named:strloadingColour )
        
    }
   
    //MARK: Show Alert
    func showAlert(_ message:String)
    {

        let alertController = UIAlertController(title: "Parsal Vendor", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .compactMap({$0 as? UIWindowScene})
                .first?.windows
                .filter({$0.isKeyWindow}).first

        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        
        let messageText = NSMutableAttributedString(
            string: message,
            attributes: [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0)
            ]
        )
        
        alertController.setValue(messageText, forKey: "attributedMessage")
        keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        
            
            
        
    }

}


extension Data{
    public func sha256() -> String{
        return hexStringFromData(input: digest(input: self as NSData))
    }
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        return hexString
    }
}
public extension String {
    func sha256() -> String{
        if let stringData = self.data(using: String.Encoding.utf8) {
            return stringData.sha256()
        }
        return ""
    }
}

extension UIView{
    func shakeTf(){
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        self.layer.add(animation, forKey: "position")
    }
    
}

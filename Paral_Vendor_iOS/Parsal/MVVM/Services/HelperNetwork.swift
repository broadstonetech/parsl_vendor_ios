//
//  HelperNetwork.swift
//  Parsal
//
//  Created by Hussnain Ali on 03/03/2022.
//

import Foundation
import UIKit
class NetworkHelper: NSObject {
    var isConnectedDevicesSuccess = false
    var isDmoatOnline:Bool = true;
    var isPausedInternet:Bool = true
    var isShowLoggedDeviceDialog = false;
    var isNotificationLocked = false
    var isNetworkAvailable:Bool  = false
    var viewAlertsPushedInNavigationStack = true
 
    var arrayVulProducts = [String]()
    var arrayVulReferences = [String]()
    var arrayVulImpact = [String]()
    
    //tab bar selection bool
    var isHomeTabSelected:Bool = true
    var isPolicyTabSelected:Bool = false
    var isAccountTabSelected:Bool = false
    var isIntercomTabSelected:Bool = false
    
    //pause internet -- mode operations
    var savedPausedDate = Date()
    var currentPausedDateTime = Date()
    var isAdvanceModeActicated:Bool  = false
    //alerts lists new UI
    
    var protocolsValuesDictForBlocking = [String: String]()


    
    var arrayTempReadings = [Double]()
    var arrayHumidityReadings = [Double]()
    var arrayAirPressureReadings = [Double]()
    var arrayTimeReadings = [String]()
    
    var arrayChannelListViewAlert = [String]()
    var arraySubscribers = [String]()
    
    var deviceToken : Data? = nil
    var FCM_DEVICE_TOKEN : String? = ""
    var uuidToken : String? = ""
    
    var tokenSavedForSettingNewPassword : String? = nil
    var usernameSavedForVerifyCode : String? = nil
    var name : String? = nil
    var email : String? = nil
    var username : String? = nil
    var password : String? = nil
    var token : String? = nil
    
    var muteSwitchState : String? = nil
    var promptSwitchState : String? = nil
    var promptUserAlert : String? = nil
    var pushNotification : Bool = false
    var appSettingsTemperatureStatus : String? = nil
    var appSettingsMode : String? = nil
    var appSettingsLEDManagement : String? = nil
    var daysSelected : String? = nil

    
    var error401 = false
    var error403 = false
    
    var mondayChecked = false
    var tuesdayChecked = false
    var wednesdayChecked = false
    var thursdayChecked = false
    var fridayChecked = false
    var saturdayChecked = false
    var sundayChecked = false
    
    var connectedHostList = false
 
    
    static let sharedInstance = NetworkHelper()
    
    
    
 
    

}

// MARK: - CONSTANTS
struct ConstantStrings {
    static var internet_off = "PARSAL is unable to reach the Internet. Please check your network connection and try again."
    static var add_Vendour_Base_Url =  "https://api.parsl.io/add/vendor/"
    static var add_Vendour_Model_texture = add_Vendour_Base_Url + "product"
    static var add_Vendour_Model_files = add_Vendour_Base_Url + "files"
    static var ERROR_TEXT_503 = "Error code 503 received from server. Please report this error if you contact support."
    static var ERROR_TEXT_409 = "Error code 409 received from server. Please report this error if you contact support."
    static var ERROR_TEXT_202 = "The 'username' is taken"
    //REsponse errors
    static var ErrorText = "Request failed. Please try again."
    static var Slogan = "Please select a thumbnail."
    static var nameSection = "Please compelete all fields in Name Section"
    static var sourceSection = "Please compelete all fields in Socurce Section"
    static var addressSection = "Please compelete all fields in Adress Section"
    static var citySectoin = "Please compelete all fields in Adress Section"
    static var catSection = "Please add category"
    static var formFill = "Fill Form Correclty"
    static var subCatSection = "Please add subCategory"
    static var imageSection = "Images are missing"
    
    
    
}


//MARK: - STRINGS
struct validString{
    

    var isValidEmail: Bool {
        return NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}").evaluate(with: self)
    }
     var numberValidation: Bool {
        return NSPredicate(format: "SELF MATCHES %@", "^(?=.*[0-9]).{10}$").evaluate(with: self)
      }
    
}
